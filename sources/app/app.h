/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#ifndef __APP_H__
#define __APP_H__

#ifdef __cplusplus
extern "C"
{
#endif
#include "ak.h"
#include "app_eeprom.h"
#include "app_data.h"

/*****************************************************************************/
/* FIRMWARE task define
 */
/*****************************************************************************/
/* define timer */
#define FW_PACKED_TIMEOUT_INTERVAL			(5000)
#define FW_CHECKING_INTERVAL				(500)

/* define signal */
enum {
	FW_CRENT_APP_FW_INFO_REQ = AK_USER_DEFINE_SIG,
	FW_CRENT_BOOT_FW_INFO_REQ,
	FW_UPDATE_REQ,
	FW_UPDATE_SM_OK,
	FW_TRANSFER_REQ,
	FW_INTERNAL_UPDATE_APP_RES_OK,
	FW_INTERNAL_UPDATE_BOOT_RES_OK,
	FW_SAFE_MODE_RES_OK,
	FW_UPDATE_SM_BUSY,
	FW_PACKED_TIMEOUT,
	FW_CHECKING_REQ
};

/*****************************************************************************/
/*  LIFE task define
 */
/*****************************************************************************/
/* define timer */
#define AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
enum {
	AC_LIFE_SYSTEM_CHECK = AK_USER_DEFINE_SIG,
};

/*****************************************************************************/
/*  SHELL task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
enum {
	AC_SHELL_LOGIN_CMD = AK_USER_DEFINE_SIG,
	AC_SHELL_REMOTE_CMD,
};

/*****************************************************************************/
/* IF task define
 */
/*****************************************************************************/
/* define timer */
#define AC_IF_TIMER_PACKET_TIMEOUT_INTERVAL			(500)

/* define signal */
enum {
	AC_IF_PURE_MSG_IN = AK_USER_DEFINE_SIG,
	AC_IF_PURE_MSG_OUT,
	AC_IF_COMMON_MSG_IN,
	AC_IF_COMMON_MSG_OUT,
	AC_IF_DYNAMIC_MSG_IN,
	AC_IF_DYNAMIC_MSG_OUT,
};

/*****************************************************************************/
/* UART_IF task define
 */
/*****************************************************************************/
/* timer signal */
#define AC_UART_IF_FRAME_TO_INTERVAL		20

/* define signal */
enum {
	AC_UART_IF_PURE_MSG_OUT = AK_USER_DEFINE_SIG,
	AC_UART_IF_COMMON_MSG_OUT,
	AC_UART_IF_DYNAMIC_MSG_OUT,
	AC_UART_IF_FRAME_TO,
};

/*****************************************************************************/
/*  LIFE task define
 */
/*****************************************************************************/
/* define timer */
#define AC_DISPLAY_LOGO_INTERVAL			(10000)

/* define signal */
enum {
	AC_DISPLAY_INITIAL = AK_USER_DEFINE_SIG,
	AC_DISPLAY_SHOW_ON_LOGO,
	AC_DISPLAY_SHOW_OFF,
};

/*****************************************************************************/
/* DBG task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	AC_DBG_TEST_1 = AK_USER_DEFINE_SIG,
	AC_DBG_TEST_2,
	AC_DBG_TEST_3
};


/*****************************************************************************/
/*  ui task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	AC_BUTTON_ONE_CLICK		,
	AC_BUTTON_LONG_PRESS	,
	AC_SENT_LEAVE_NETWORK	,
	AC_PINGPONG_SENT_TEST	,
	AC_LORAWAN_SENT_TEST	,
};

/*****************************************************************************/
/*  loramac task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	LORAMAC_RADIO_TXTIMEOUT			,
	LORAMAC_RADIO_RXTIMEOUT			,
	LORAMAC_RADIO_RXTIMEOUT_SYNCWORD,
	LORAMAC_MAC_STATE_CHECK_TIMER	,
	LORAMAC_MAC_TXDELAYED_TIMER		,
	LORAMAC_MAC_RXWINDOW_1_TIMER	,
	LORAMAC_MAC_RXWINDOW_2_TIMER	,
	LORAMAC_MAC_ACK_TIMEOUT			,
	LORAMAC_MAC_SEND_TEST			,
};

/*****************************************************************************/
/*  sensor task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	SENSOR_INIT,
	SENSOR_PWR_ON,
	SENSOR_CONVERT_REQ,
	SENSOR_READ,
};

/*****************************************************************************/
/*  alarm task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	ALARM_RECV_MSG,
	ALARM_ON,
	ALARM_OFF,
};

/*****************************************************************************/
/*  setting task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	SETTING_RECV_MSG,
};

/*****************************************************************************/
/* CMD_UART task define
 */
/*****************************************************************************/
/* timer signal */
#define CMD_UART_FRAME_TO_INTERVAL		500

/* define signal */
enum {
	CMD_UART_SET_MODE = AK_USER_DEFINE_SIG,
	CMD_UART_FRAME_TO = 0xFF,
};

/*****************************************************************************/
/*  enddevice task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	/*signal triger from external*/
	ENDDEVICE_SET_ACTIVE_MODE = AK_USER_DEFINE_SIG,
	ENDDEVICE_SET_DEV_EUI ,
	ENDDEVICE_SET_APP_EUI,
	ENDDEVICE_SET_APP_KEY,
	ENDDEVICE_SET_NET_ID,
	ENDDEVICE_SET_DEV_ADDR,
	ENDDEVICE_SET_NKW_SKEY,
	ENDDEVICE_SET_APP_SKEY,
	ENDDEVICE_SET_CLASS,
	ENDDEVICE_SET_CHAN_MASK,
	ENDDEVICE_SET_DATARATE,
	ENDDEVICE_SET_RX2_CHAN,
	ENDDEVICE_SET_RX2_DR,
	ENDDEVICE_SET_TX_PWR,
	ENDDEVICE_SET_ANT_GAIN,
	ENDDEVICE_SET_PORT,
	ENDDEVICE_GET_DEV_EUI,
	ENDDEVICE_GET_APP_EUI,
	ENDDEVICE_GET_APP_KEY,
	ENDDEVICE_GET_NET_ID,
	ENDDEVICE_GET_DEV_ADDR,
	ENDDEVICE_GET_NKW_SKEY,
	ENDDEVICE_GET_APP_SKEY,
	ENDDEVICE_GET_CLASS,
	ENDDEVICE_GET_CHAN_MASK,
	ENDDEVICE_GET_DATARATE,
	ENDDEVICE_GET_RX2_CHAN,
	ENDDEVICE_GET_RX2_DR,
	ENDDEVICE_GET_TX_PWR,
	ENDDEVICE_GET_ANT_GAIN,
	ENDDEVICE_GET_PORT,
	ENDDEVICE_SEND_UNCONFIRM_MSG,
	ENDDEVICE_SEND_CONFIRM_MSG,
};

/*****************************************************************************/
/*  forward task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	/*signal triger from external*/
	FORWARD_SET_TX_CONFIG = AK_USER_DEFINE_SIG,
	FORWARD_GET_TX_CONFIG,
	FORWARD_SET_RX_CONFIG,
	FORWARD_GET_RX_CONFIG,
	FORWARD_SEND_MSG,
};


/*****************************************************************************/
/*  server task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	/*signal triger from external*/
	SERVER_SET_TX_CONFIG = AK_USER_DEFINE_SIG,
	SERVER_GET_TX_CONFIG,
	SERVER_SET_RX_CONFIG,
	SERVER_GET_RX_CONFIG,
	SERVER_SET_NET_PARAM,
	SERVER_GET_NET_PARAM,
	SERVER_SET_JOINT_PERMIT,
	SERVER_GET_JOINT_PERMIT,
	SERVER_REMOVE_DEVICE,
	SERVER_REMOVE_DEVICE_ALL,
	SERVER_SEND_UNCONFIRM_MSG,
	SERVER_SEND_CONFIRM_MSG,

	/*signal internal*/
	SERVER_RECV_JOIN_MSG,
	SERVER_SEND_JOIN,
	SERVER_JOIN_TIMEOUT,
	SERVER_RECV_UNCONFIRM_MSG,
	SERVER_RECV_CONFIRM_MSG,
	SERVER_SEND_MSG_RETRY,
};

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK									(0x00)
#define APP_NG									(0x01)

#define APP_FLAG_OFF							(0x00)
#define APP_FLAG_ON								(0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
#define APP_VER		"0.0.1"

extern const char* app_version;

extern void* app_get_boot_share_data();
extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //__APP_H__

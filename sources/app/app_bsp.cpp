#include "button.h"

#include "sys_dbg.h"

#include "app.h"
#include "app_bsp.h"
#include "app_dbg.h"

#include "task_list.h"

button_t button;

void button_callback(void* b) {
	button_t* me_b = (button_t*)b;
	switch (me_b->state) {
	case BUTTON_SW_STATE_RELEASED: {
		APP_DBG("[btn_callback] BUTTON_SW_STATE_RELEASED\n");
	}
		break;

	case BUTTON_SW_STATE_PRESSED: {
		APP_DBG("[btn_callback] BUTTON_SW_STATE_SHORT_RELEASE_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_LONG_PRESSED: {
		APP_DBG("[btn_callback] BUTTON_SW_STATE_LONG_PRESSED\n");
	}
		break;

	default:
		break;
	}
}


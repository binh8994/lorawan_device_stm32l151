/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#include <stdint.h>
#include <stdlib.h>

#include "ak.h"
#include "task.h"
#include "timer.h"
#include "message.h"

#include "cmd_line.h"
#include "utils.h"
#include "xprintf.h"
#include "view_render.h"

#include "sys_ctrl.h"
#include "sys_io.h"
#include "sys_dbg.h"
#include "sys_irq.h"
#include "sys_svc.h"
#include "sys_arduino.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"
#include "app_flash.h"
#include "app_eeprom.h"
#include "app_non_clear_ram.h"

#include "task_shell.h"
#include "task_list.h"
#include "task_life.h"

#include "rtc.h"
#include "led.h"
#include "eeprom.h"
#include "Adafruit_ssd1306syp.h"
#include "EmonLib.h"
#include "DS1302.h"
#include "flash.h"
#include "hs1101.h"
#include "exor.h"

#include "WString.h"
#include "Wire.h"

#include "lorawan_endd.h"

/*****************************************************************************/
/*  local declare
 */
/*****************************************************************************/
#define STR_LIST_MAX_SIZE		50
#define STR_BUFFER_SIZE			500

#if 0
static char cmd_buffer[STR_BUFFER_SIZE];
static char* str_list[STR_LIST_MAX_SIZE];
static uint8_t str_list_len;

static uint8_t str_parser(char* str);
static char* str_parser_get_attr(uint8_t);
#endif

/*****************************************************************************/
/*  command function declare
 */
/*****************************************************************************/
static int32_t shell_reset(uint8_t* argv);
static int32_t shell_ver(uint8_t* argv);
static int32_t shell_help(uint8_t* argv);
static int32_t shell_reboot(uint8_t* argv);
static int32_t shell_dbg(uint8_t* argv);
static int32_t shell_lora(uint8_t* argv);

/*****************************************************************************/
/*  command table
 */
/*****************************************************************************/
cmd_line_t lgn_cmd_table[] = {

	/*************************************************************************/
	/* system command */
	/*************************************************************************/
	{(const int8_t*)"reset",	shell_reset,		(const int8_t*)"reset terminal"},
	{(const int8_t*)"ver",		shell_ver,			(const int8_t*)"version info"},
	{(const int8_t*)"help",		shell_help,			(const int8_t*)"help info"},
	{(const int8_t*)"reboot",	shell_reboot,		(const int8_t*)"reboot"},
	{(const int8_t*)"lora",		shell_lora,			(const int8_t*)"lora setting"},

	/*************************************************************************/
	/* debug command */
	/*************************************************************************/
	{(const int8_t*)"dbg",		shell_dbg,			(const int8_t*)"dbg"},

	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

#if 0
uint8_t str_parser(char* str) {
	strcpy(cmd_buffer, str);
	str_list_len = 0;

	uint8_t i = 0;
	uint8_t str_list_index = 0;
	uint8_t flag_insert_str = 1;

	while (cmd_buffer[i] != 0 && cmd_buffer[i] != '\n' && cmd_buffer[i] != '\r') {
		if (cmd_buffer[i] == ' ') {
			cmd_buffer[i] = 0;
			flag_insert_str = 1;
		}
		else if (flag_insert_str) {
			str_list[str_list_index++] = &cmd_buffer[i];
			flag_insert_str = 0;
		}
		i++;
	}

	cmd_buffer[i] = 0;

	str_list_len = str_list_index;
	return str_list_len;
}

char* str_parser_get_attr(uint8_t index) {
	if (index < str_list_len) {
		return str_list[index];
	}
	return NULL;
}
#endif

/*****************************************************************************/
/*  command function definaion
 */
/*****************************************************************************/
int32_t shell_reset(uint8_t* argv) {
	(void)argv;
	xprintf("\033[2J\r");
	return 0;
}

int32_t shell_ver(uint8_t* argv) {
	(void)argv;

	firmware_header_t firmware_header;
	sys_ctrl_get_firmware_info(&firmware_header);

	LOGIN_PRINT("Kernel version: %s\n", AK_VERSION);
	LOGIN_PRINT("App version: %s\n", app_version);
	LOGIN_PRINT("Firmware checksum: 0x%04x\n", firmware_header.checksum);
	LOGIN_PRINT("Firmware length: %d\n", firmware_header.bin_len);

	LOGIN_PRINT("\nSystem information:\n");
	LOGIN_PRINT("\tFLASH used:\t%d bytes\n", system_info.flash_used);
	LOGIN_PRINT("\tSRAM used:\t%d bytes\n", system_info.ram_used);
	LOGIN_PRINT("\t\tdata init size:\t\t%d bytes\n", system_info.data_init_size);
	LOGIN_PRINT("\t\tdata non_init size:\t%d bytes\n", system_info.data_non_init_size);
	LOGIN_PRINT("\t\tstack avail:\t\t%d bytes\n", system_info.stack_avail);
	LOGIN_PRINT("\t\theap avail:\t\t%d bytes\n", system_info.heap_avail);
	LOGIN_PRINT("\t\tother:\t\t\t%d bytes\n", system_info.ram_other);
	LOGIN_PRINT("\n");
	LOGIN_PRINT("\tcpu clock:\t%d Hz\n", system_info.cpu_clock);
	LOGIN_PRINT("\ttime tick:\t%d ms\n", system_info.tick);
	LOGIN_PRINT("\tconsole:\t%d bps\n", system_info.console_baudrate);
	LOGIN_PRINT("\n\n");
	return 0;
}

int32_t shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		LOGIN_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			LOGIN_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t shell_reboot(uint8_t* argv) {
	(void)argv;
	sys_ctrl_reset();
	return 0;
}

int32_t shell_dbg(uint8_t* argv) {
	(void)(argv);
	switch (*(argv + 4)) {
	case '1': {
	}
		break;

	default:
		break;
	}

	return 0;
}

int32_t shell_lora(uint8_t* argv) {
	endd_setting_t lsetting;

	switch (*(argv + 5)) {
	case 'o': {

		uint8_t app_Eui[8] = {1, 2, 3, 4, 5, 6, 7, 8};
		uint8_t app_key[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

		eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(endd_setting_t));

		memset(&lsetting, 0, sizeof(endd_setting_t));
		lsetting.network_joined = LORAWAN_NWK_FREE;
		get_device_Eui(lsetting.device_Eui);
		memcpy(lsetting.application_Eui, app_Eui, 8);
		memcpy(lsetting.application_key, app_key, 16);

		LOGIN_PRINT("writing...\n");
		eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(endd_setting_t));
		LOGIN_PRINT("done\n");
	}
		break;

	case 'a': {

		uint8_t app_Eui[8] = {1, 2, 3, 4, 5, 6, 7, 8};
		uint8_t app_key[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

		eeprom_read(ENDD_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(endd_setting_t));

		memset(&lsetting, 0, sizeof(endd_setting_t));
		lsetting.network_joined = LORAWAN_NWK_JOINED;
		get_device_Eui(lsetting.device_Eui);
		memcpy(lsetting.application_Eui, app_Eui, 8);
		memcpy(lsetting.application_key, app_key, 16);
		lsetting.network_id = 0;
		lsetting.device_address = 0xFFFFFFFF;
		memcpy(lsetting.application_skey, app_key, 16);
		memcpy(lsetting.network_skey, app_key, 16);

		LOGIN_PRINT("writing...\n");
		eeprom_write(ENDD_SETTING_ADDR, (uint8_t*)&lsetting, sizeof(endd_setting_t));
		LOGIN_PRINT("done\n");
	}
		break;

	default:
		LOGIN_PRINT("lora <..>\n");
		LOGIN_PRINT(" t\tsend test\n");
		LOGIN_PRINT(" o\tsetup OTA\n");
		LOGIN_PRINT(" a\tsetup ABP\n");
		break;
	}

	return 0;
}


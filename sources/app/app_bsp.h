#ifndef __APP_BSP_H__
#define __APP_BSP_H__

#include "button.h"

#define BUTTON_ID					(0x01)

extern button_t button;

extern void button_callback(void*);

#endif //__APP_BSP_H__

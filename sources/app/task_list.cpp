#include "task_list.h"
#include "timer.h"

task_t app_task_table[] = {
	/*************************************************************************/
	/* SYSTEM TASK */
	/*************************************************************************/
	{TASK_TIMER_TICK_ID,		TASK_PRI_LEVEL_7,		task_timer_tick		},

	/*************************************************************************/
	/* APP TASK */
	/*************************************************************************/
	{AC_TASK_SHELL_ID		,	TASK_PRI_LEVEL_2	,	task_shell			},
	{AC_TASK_LIFE_ID		,	TASK_PRI_LEVEL_6	,	task_life			},
	{AC_TASK_LORAMAC_ID		,	TASK_PRI_LEVEL_5	,	task_loramac		},
	/*************************************************************************/
	/* RF24 NETWORK TASK */
	/*************************************************************************/
	//{RF24_PHY_ID			,	TASK_PRI_LEVEL_3	,	task_rf24_phy		},
	//{RF24_MAC_ID			,	TASK_PRI_LEVEL_4	,	task_rf24_mac		},
	//{RF24_NWK_ID			,	TASK_PRI_LEVEL_5	,	task_rf24_nwk		},

	/*************************************************************************/
	/* LINK TASK */
	/*************************************************************************/
	//{LINK_PHY_ID			,	TASK_PRI_LEVEL_3	,	task_link_phy		},
	//{LINK_MAC_ID			,	TASK_PRI_LEVEL_4	,	task_link_mac		},

	/*************************************************************************/
	/* END OF TABLE */
	/*************************************************************************/
	{AK_TASK_EOT_ID,			TASK_PRI_LEVEL_0,		(pf_task)0			}
};

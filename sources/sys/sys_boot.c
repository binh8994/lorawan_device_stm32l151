#include <string.h>

#include "sys_boot.h"
#include "sys_io.h"
#include "sys_dbg.h"

//static sys_boot_t sys_boot_obj;

void sys_boot_init() {
}

void sys_boot_get(sys_boot_t* obj) {
	(void)obj;
}

uint8_t sys_boot_set(sys_boot_t* sys_boot) {
	(void)sys_boot;
	return SYS_BOOT_OK;
}
